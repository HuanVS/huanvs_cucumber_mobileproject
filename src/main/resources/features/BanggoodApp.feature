Feature:Banggood TestSuite

  Scenario Outline: TC1_Verify product screen detail
    When User click to the Category on footer menu
    And Scroll and click to Home and Garden in Left menu
    And After the right category displayed, click to the “Home Decor”
    And Input price from <Min> to <Max> for filter
    And User click first product
    Then User should see product name displayed and product price in range <Min> to <Max>
    Examples:
      | Min | Max |
      | 20       | 30       |

  Scenario: TC2_Verify Categories detail screen
    When User scroll to Hot Categories at Home screen and click to first category
    And User click first product
    And After product detail displayed , user click Add to cart
    And User click to Cart icon on header
    Then User should see product name, product size, product price and quantity

  Scenario: TC3_Verify All order screen
    When User click Account on footer menu
    And User click View All Order
    Then The login screen should be displayed with: Email, password and Sign In button

  Scenario: TC4_ Verify Computer & Office detail screen
    When User click to Computer & Office
    And  User click to Computer Components
    And  User click to first product
    Then Detail product screen should be displayed with: Buy Now, Add to Cart button

  Scenario: TC5_Verify button Buy Now and Add to Cart on Categories detail screen
    When User scroll to Hot Categories at Home screen and click to first category
    And User click first product
    And After product detail displayed , user click Add to cart
    And User click to Cart icon on header
    Then User should see Buy Now, Add to Cart button
