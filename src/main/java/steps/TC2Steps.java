package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;

public class TC2Steps {
    Locators locators = new Locators();
    AndroidKeywords keywords= new AndroidKeywords();

    @When("User scroll to Hot Categories at Home screen and click to first category")
    public void scrollAndClickToFirstCategory(){
        keywords.waitToClickAble(locators.category,20);
        keywords.scrollToElementByPercent(locators.hotCategory,2);
        keywords.clickElementByXpath(locators.firstHotCategory);
    }

    @And("After product detail displayed , user click Add to cart")
    public void verifyProductDetailAndClickAddToCart(){
        keywords.waitUntilPageContainsElement(locators.productName);
        keywords.verifyElementByXpath(locators.productName);
        keywords.verifyElementByXpath(locators.productPrice);
        keywords.verifyElementByXpath(locators.addToCartButton);
        keywords.verifyElementByXpath(locators.buyNowButton);
        keywords.clickElementByXpath(locators.addToCartButton);
    }

    @And("User click to Cart icon on header")
    public void clickToCartIconHeader(){
        keywords.waitToClickAble(locators.cartOnFooter,20);
        keywords.clickElementByXpath(locators.cartOnFooter);
        keywords.waitToClickAble(locators.cartIconOnFooter,20);
        keywords.clickElementByXpath(locators.cartIconOnFooter);
    }

    @And("User should see product name, product size, product price and quantity")
    public void verifyOrderInfo(){
        keywords.waitUntilPageContainsElement(locators.productName);
        keywords.verifyElementByXpath(locators.productName);
        keywords.verifyElementByXpath(locators.productPrice);
        keywords.verifyElementByXpath(locators.quantity);
        keywords.goBack();
        keywords.goBack();
        keywords.goBack();
    }
}
