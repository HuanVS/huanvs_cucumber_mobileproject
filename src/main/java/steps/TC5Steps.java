package steps;

import io.cucumber.java.en.Then;
import keywords.AndroidKeywords;
import locators.Locators;

public class TC5Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @Then("User should see Buy Now, Add to Cart button")
    public void userShouldSeeBuyNowAddToCartButton() {
        keywords.waitUntilPageContainsElement(locators.buyNowButton);
        keywords.verifyElementByXpath(locators.buyNowButton);
        keywords.verifyElementByXpath(locators.addToCartButton);
    }
}
