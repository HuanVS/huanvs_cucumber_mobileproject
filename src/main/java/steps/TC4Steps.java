package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;

public class TC4Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @When("User click to Computer & Office")
    public void userClickToComputerOffice() {
        keywords.waitUntilPageContainsElement(locators.category);
        keywords.clickElementByXpath(locators.category);
        keywords.waitUntilPageContainsElement(locators.computerAndOffice);
        keywords.clickElementByXpath(locators.computerAndOffice);
    }

    @And("User click to Computer Components")
    public void userClickToComputerComponents() {
        keywords.waitUntilPageContainsElement(locators.computerAndComponent);
        keywords.clickElementByXpath(locators.computerAndComponent);
    }

    @And("User click to first product")
    public void userClickToFirstProduct() {
        keywords.waitToClickAble(locators.firstProduct2, 20);
        keywords.clickElementByXpath(locators.firstProduct2);
    }

    @Then("Detail product screen should be displayed with: Buy Now, Add to Cart button")
    public void detailProductScreenShouldBeDisplayedWithBuyNowAddToCartButton() {
        keywords.waitUntilPageContainsElement(locators.buyNowButton);
        keywords.verifyElementByXpath(locators.buyNowButton);
        keywords.verifyElementByXpath(locators.addToCartButton);
        keywords.goBack();
        keywords.goBack();
    }

}
