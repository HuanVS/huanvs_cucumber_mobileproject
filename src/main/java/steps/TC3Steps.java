package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;

public class TC3Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @When("User click Account on footer menu")
    public void clickToAccount(){
        keywords.waitUntilPageContainsElement(locators.accountTab);
        keywords.clickElementByXpath(locators.accountTab);
    }

    @And("User click View All Order")
    public void clickToViewAllOrder(){
        keywords.waitUntilPageContainsElement(locators.viewAllOrder);
        keywords.clickElementByXpath(locators.viewAllOrder);
    }

    @Then("The login screen should be displayed with: Email, password and Sign In button")
    public void verifyLoginPage(){
        keywords.waitUntilPageContainsElement(locators.emailField);
        keywords.verifyElementByXpath(locators.emailField);
        keywords.verifyElementByXpath(locators.passwordField);
        keywords.verifyElementByXpath(locators.signInButton);
        keywords.goBack();
        keywords.goBack();
    }
}
