import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import keywords.AndroidKeywords;
import locators.Locators;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.net.MalformedURLException;

public class ServiceHooks {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @Before
    public void before() throws MalformedURLException {
        keywords.openApplication(locators.platformName, locators.platformVersion, locators.udid, locators.appPackage, locators.appActivity);
        try{
            keywords.waitUntilPageContainsElement(locators.popUp);
            keywords.clickElementByXpath(locators.closePopUpBtn);
        }catch (Exception e){}
    }

    @After
    public void afterHooks(Scenario scenario) {
        if (scenario.isFailed()) {
            scenario.attach(((TakesScreenshot) AndroidKeywords.driver).getScreenshotAs(OutputType.BYTES),
                    "image/png", "image");
            AndroidKeywords.driver.closeApp();
            AndroidKeywords.driver = null;
        }

    }

}
