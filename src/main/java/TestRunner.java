import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/main/resources/features/BanggoodApp.feature"
)
public class TestRunner extends AbstractTestNGCucumberTests {
}
