package keywords;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidKeywords {
    public AndroidKeywords() {
    }
    public static String userName = "vngshun_txcHPN";
    public static String accessKey = "6GVSQdGuhYhzWawoDkji";
    public static AndroidDriver<?> driver;
    public final int TIMEOUT = 20;

    /**
     * Mobile Web App actions
     */
    public void clickWebElement(String locator) {
        WebElement we = driver.findElement(By.xpath(locator));
        we.click();
    }

    public void goToUrl(String url) {
        driver.navigate().to(url);
    }

    public void pressEnter() {
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    public void quitSesson() {
        driver.quit();
    }

    /**
     * Native/Hybrid App actions
     */
    /**
     * Interactive actions
     */
    public void openApplication(String platformName, String platformVersion, String udid, String appPackage, String appActivity) throws MalformedURLException {
        if (driver == null) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("device", "Samsung Galaxy S22 Ultra");
            caps.setCapability("os_version", "12.0");
            caps.setCapability("project", "Banggood");
            caps.setCapability("build", "My First Build");
            caps.setCapability("name", "Bstack-[Java] Sample Test");
            caps.setCapability("app", "bs://82762413a61eba7561b26f6e9c56764c664383be");

            driver = new AndroidDriver<AndroidElement>(new URL("https://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"), caps);
        }
    }

    public void closeApplication() {
        driver.quit();
    }

    public void clickElementByXpath(String locator) {
        WebElement we = driver.findElement(By.xpath(locator));
        TouchAction action = new TouchAction(driver);
        action.tap(TapOptions.tapOptions().withElement(ElementOption.element(we))).perform();
    }

    public void clickElementByID(String locator) {
        WebElement we = driver.findElement(By.id(locator));
        TouchAction action = new TouchAction(driver);
        action.tap(TapOptions.tapOptions().withElement(ElementOption.element(we))).perform();
    }

    public void inputText(String locator, String text) {
        WebElement we = driver.findElement(By.xpath(locator));
        we.sendKeys(text);
    }

    public WebElement getElementByXpath(String locator) {
        WebElement we = driver.findElement(By.xpath(locator));
        return we;
    }

    public String getText(String locator) {
        WebElement we = driver.findElement(By.xpath(locator));
        String text = we.getText();
        return text;
    }

    public void goBack() {
        driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
    }

    public void swipe(int startx, int starty, int endx, int endy) {
        new TouchAction(driver).longPress(PointOption.point(startx, starty))
                .moveTo(PointOption.point(endx, endy))
                .release().perform();
    }

    public void swipeMobileUp(String locator) {
        Dimension size = driver.findElement(By.xpath(locator)).getSize();
        int starty = (int) (size.height * 0.8);
        int endy = (int) (size.height * 0.2);
        int startx = size.width / 2;
        swipe(startx, starty, startx, endy);
    }

    public void scrollToElement(String locatorMenu, String locator) {
        boolean flag = true;
        WebElement webElement = null;
        do {
            try {
                webElement = driver.findElement(By.xpath(locator));
            } catch (Exception e) {
            }
            if (webElement == null) {
                swipeMobileUp(locatorMenu);
            } else {
                flag = false;
            }
        } while (flag);
    }

    public void swipeMobileUpByPercent(int x) {
        Dimension size = driver.manage().window().getSize();
        int starty = (int) (size.height * 0.8);
        int endy = (int) (size.height * 0.2);
        int startx = size.width / 2;
        swipe(startx, starty, startx, endy);
    }

    public void scrollToElementByPercent(String locator, int with) {
        boolean flag = true;
        WebElement webElement = null;
        do {
            try {
                webElement = driver.findElement(By.xpath(locator));
            } catch (Exception e) {
            }
            if (webElement == null) {
                swipeMobileUpByPercent(with);
            } else {
                flag = false;
            }
        } while (flag);
    }

    public void tapElementByCoordinates(int x, double y) {
        Dimension size = driver.manage().window().getSize();
        int width = size.width / x;
        int height = (int) (size.height * y);
        TouchAction action = new TouchAction(driver);
        action.tap(TapOptions.tapOptions().withPosition(PointOption.point(width, height))).perform();
    }

    public void sendKeyByXpath(String locator, String text) {
        WebElement we = driver.findElement(By.xpath(locator));
        we.sendKeys(text);
    }

    /**
     * Verification actions
     */
    public boolean elementTextShouldBe(String locator, String expected) {
        WebElement we = driver.findElement(By.xpath(locator));
        String strActual = we.getText();
        boolean blnCompare = strActual.equals(expected);
        if (!blnCompare) {
            System.out.println("Actual text " + strActual + " and expected text " + expected + " of the element are NOT matched");
            return false;
        } else {
            System.out.println("The element contains exactly text " + expected);
            return true;
        }
    }

    public void pageShouldContainText(String expected) {
        String bodyText = driver.findElement(By.cssSelector("BODY")).getText();
        Assert.assertTrue(bodyText.contains(expected), "Text not found!");
    }

    public void verifyElementByXpath(String locator) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, TIMEOUT);
        WebElement we = driver.findElement(By.xpath(locator));
        Assert.assertTrue(webDriverWait.until(ExpectedConditions.visibilityOf(we)).isDisplayed());
    }

    public boolean verifyProductPrice(String locator, int minPrice, int maxPrice) {
        WebElement we = driver.findElement(By.xpath(locator));
        String price = (we.getText()).substring(3);
        double price1 = Double.valueOf(price);
        Double doubleValue = price1;
        int price2 = doubleValue.intValue();
        if (minPrice < price2 && price2 < maxPrice) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Wait actions
     */
    public void waitUntilPageContainsElement(String locator) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public void waitUntilElementInvisible(String locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
    }

    public void waitToClickAble(String locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
    }


}
