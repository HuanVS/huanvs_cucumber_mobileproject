package locators;

public class Locators {
    public String platformName = "Android";
    public String platformVersion = "7";
    public String udid = "emulator-5554";
    public String appPackage = "com.banggood.client";
    public String appActivity = "com.banggood.client.Launcher";

    //Scenario 1
    public String homePage ="//android.widget.TextView[@text='Home']/..";
    public String popUp ="//android.widget.ImageView[@resource-id='com.banggood.client:id/iv_new_user_exclusive']";
    public String closePopUpBtn = "//android.widget.ImageView[@resource-id='com.banggood.client:id/iv_close']";
    public String category = "//android.view.ViewGroup[@resource-id='com.banggood.client:id/main_tab_category']";
    public String rightMenu ="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.banggood.client:id/secondary_cate_list']";
    public String leftMenu = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.banggood.client:id/primary_cate_list']";
    public String homeAndGarden ="//android.widget.TextView[@text='Home and Garden']/..";
    public String homeDecor ="//android.widget.TextView[@text='Home Decor']/..";
    public String filterButton = "//android.view.ViewGroup[@resource-id='com.banggood.client:id/slide_filter_entry']";
    public String minPriceEdit ="//android.widget.EditText[@resource-id='com.banggood.client:id/edit_price_min']";
    public String maxPriceEdit ="//android.widget.EditText[@resource-id='com.banggood.client:id/edit_price_max']";
    public String doneButton ="//android.widget.Button[@resource-id='com.banggood.client:id/btn_done']";

    public String firstProduct ="(//android.widget.FrameLayout[@resource-id='com.banggood.client:id/item_view'])[1]";
    public String productName ="//android.widget.TextView[@resource-id='com.banggood.client:id/tv_product_name']";
    public String productPrice ="//android.widget.TextView[@resource-id='com.banggood.client:id/tv_product_price']";

    //Scenario 2
    public String hotCategory="//android.widget.TextView[@text='Hot Categories']/..";
    public String firstHotCategory ="(//androidx.recyclerview.widget.RecyclerView[@resource-id='com.banggood.client:id/rv_hot_categories']/child::android.widget.FrameLayout)[1]";
    public String buyNowButton ="//android.widget.Button[@resource-id='com.banggood.client:id/btn_slide_buy']";
    public String addToCartButton ="//android.widget.Button[@resource-id='com.banggood.client:id/btn_slide_cart']";
    public String cartOnFooter ="//android.widget.Button[@resource-id='com.banggood.client:id/btn_cart']";
    public String cartIconOnFooter ="//android.widget.FrameLayout[@resource-id='com.banggood.client:id/cv_slide_cart']";
    public String quantity ="//android.widget.LinearLayout[@resource-id='com.banggood.client:id/qty_edit_view']";

    //Scenario 3
    public String accountTab ="//android.view.ViewGroup[@resource-id='com.banggood.client:id/main_tab_account']";
    public String viewAllOrder ="//android.widget.TextView[@resource-id='com.banggood.client:id/tv_view_all']";
    public String emailField="//android.widget.LinearLayout[@resource-id='com.banggood.client:id/til_email']";
    public String passwordField= "//android.widget.LinearLayout[@resource-id='com.banggood.client:id/til_pwd']";
    public String signInButton= "//android.widget.Button[@resource-id='com.banggood.client:id/btn_sign']";

    //TC 4
    public String computerAndOffice = "//android.widget.TextView[@text='Computers & Office']";
    public String computerAndComponent = "//android.widget.TextView[@text='Computer Components']";
    public String firstProduct2 = "(//android.widget.FrameLayout[@resource-id='com.banggood.client:id/item_view'])[1]";
    public String buyNowBtn = "//android.widget.Button[@resource-id='com.banggood.client:id/btn_slide_buy']";
    public String addToCartBtn = "//android.widget.Button[@resource-id='com.banggood.client:id/btn_slide_cart']";
}
